package com.itheima.生产者消费者;

public class Test {
    public static void main(String[] args) {
        Desk desk = new Desk(false,10,new Object());
        Foodie foodie = new Foodie(desk);
        Cooker cooker = new Cooker(desk);
        foodie.start();
        cooker.start();
    }
}
