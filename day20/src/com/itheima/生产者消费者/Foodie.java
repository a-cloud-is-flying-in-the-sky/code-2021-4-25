package com.itheima.生产者消费者;

public class Foodie extends Thread{
    private Desk desk;
    public Foodie(Desk desk) {
        this.desk=desk;
    }

    @Override
    public void run() {
        while (true){
            synchronized (desk.getLock()){//多线程操作共享数据时要使用锁
                if (desk.getCount()==0){
                    break;
                }else{
                    if (desk.isFlag()){//判断执行条件
                        System.out.println("吃");
                        desk.setFlag(false);
                        desk.setCount(desk.getCount()-1);
                        desk.getLock().notifyAll();
                    }else{
                        //注意:使用什么对象当做锁,那么就用什么对象调用等待和唤醒方法(用所对象调用)
                        try {
                            desk.getLock().wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }
}
