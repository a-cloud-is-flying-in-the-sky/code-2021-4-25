package com.itheima.生产者消费者;

public class Desk {
    /*public static boolean flag=false;//记录桌子上是否有东西(生产者消费者的等待条件)
    public static int count=10;//执行数量
    public static final Object lock=new Object();*/
    private boolean flag;
    private int count;
    private Object lock;

    public Desk() {
    }

    public Desk(boolean flag, int count, Object lock) {
        this.flag = flag;
        this.count = count;
        this.lock = lock;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Object getLock() {
        return lock;
    }

    public void setLock(Object lock) {
        this.lock = lock;
    }
}
