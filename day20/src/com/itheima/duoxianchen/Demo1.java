package com.itheima.duoxianchen;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class Demo1 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        /*Mythread m1 = new Mythread();
        Mythread m2 = new Mythread();*/
        /*MyRunnable mr = new MyRunnable();
        Thread thread1 = new Thread(mr,"xx");
        Thread thread2 = new Thread(mr,"xx");
        thread1.start();
        thread2.start();*/
        //m1.run();
        //m2.run();
        /*m1.start();
        m2.start();*/
        MyCallable mc = new MyCallable();
        FutureTask<String> ft = new FutureTask<String>(mc);
        FutureTask<String> ft1 = new FutureTask<String>(mc);
        Thread thread = new Thread(ft);
        Thread thread1 = new Thread(ft1);
        thread.start();
        thread1.start();
        String s = ft.get();
        System.out.println(s);
    }
}
