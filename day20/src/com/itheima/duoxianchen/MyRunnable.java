package com.itheima.duoxianchen;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MyRunnable implements Runnable {
    private int count=1000;
    private String string="姥姥";
    private Lock lock=new ReentrantLock();
    @Override
    public void run() {
        for (;;) {
            try {
                lock.lock();
                if (count>0){
                    count--;
                    System.out.println(Thread.currentThread().getName()+"执行,还剩:"+count);
                }else{
                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }

        }
    }
}
