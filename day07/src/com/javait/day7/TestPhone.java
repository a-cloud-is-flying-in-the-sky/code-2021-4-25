package com.javait.day7;

public class TestPhone {
    public static void main(String[] args) {
        Phone phone1 = new Phone();
        Phone phone2 = new Phone();
        Phone phone3 = new Phone();
        phone1.setBrand("苹果");//赋值用set,使用值用get
        phone1.setPrice(8999);
        phone2.setBrand("小米");
        phone2.setPrice(4999);
        phone3.setBrand("华为");
        phone3.setPrice(3599);
        phone1.show();
        phone1.call();
        phone2.show();
        phone2.playGame();
        phone3.show();
        phone3.sendMessage();

    }
}
