package com.javait.day7;

public class Teacher {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    private String name;
    private int age;
    private String content;

    public Teacher(String name, int age, String content) {
        this.name = name;
        this.age = age;
        this.content = content;
    }

    public void eat(){
        System.out.println("年龄为"+this.getAge()+"的"+this.getName()+"正在吃饭");
    }
    public void speak(){
        System.out.println("年龄为"+this.getAge()+"的"+this.getName()+"正在讲"+this.content);
    }
}
