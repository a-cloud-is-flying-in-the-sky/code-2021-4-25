package com.javait.day7;

public class Coder {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    private String name;
    private int id;

    public Coder(String name, int id, int salary) {
        this.name = name;
        this.id = id;
        this.salary = salary;
    }
    public void show(){
        System.out.print("工号为"+this.id+"基本工资为"+this.salary+"的"+this.name);
    }

    private int salary;
    public void work(){
        System.out.println("正在努力的敲代码....");
    }
}
