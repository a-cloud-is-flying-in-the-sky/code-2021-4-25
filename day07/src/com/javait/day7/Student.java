package com.javait.day7;

public class Student {
    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Student(String name, int age, String content) {
        this.name = name;
        this.age = age;
        this.content = content;
    }

    private String content;
    public void eat(){
        System.out.println("年龄为"+this.getAge()+"的"+this.getName()+"正在吃饭");
    }
    public void speak(){
        System.out.println("年龄为"+this.getAge()+"的"+this.getName()+"正在认真的"+this.content);
    }
}
