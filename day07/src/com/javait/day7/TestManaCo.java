package com.javait.day7;

public class TestManaCo {
    public static void main(String[] args) {
        Manager manager = new Manager("项目经理",38,14000,5000);
        Coder coder = new Coder("程序员",26,8000);
        manager.show();
        manager.work();
        coder.show();
        coder.work();
    }
}
