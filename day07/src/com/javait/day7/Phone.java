package com.javait.day7;

public class Phone {
    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    private String brand;
    private int price;
    public void call(){
        System.out.println("打电话...");
    }
    public void sendMessage(){
        System.out.println("发信息...");
    }
    public void playGame(){
        System.out.println("玩游戏...");
    }
    public void show(){
        System.out.print("正在使用价格为"+this.price+"元的"+this.brand+"手机");
    }
}
