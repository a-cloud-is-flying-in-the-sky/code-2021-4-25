package com.day4;

import java.util.Random;
import java.util.Scanner;

public class Day4zuoye {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Random r = new Random();
        System.out.println("请输入您要录入的数据长度:");
        int num=sc.nextInt();
        int[] arr = new int[num];
        for (int i = 0; i <num; i++) {
            arr[i]=r.nextInt(10);
        }
        System.out.print("数组元素为:");
        System.out.print("{");
        for (int i = 0; i < arr.length; i++) {
           if (i==arr.length-1){
               System.out.print(arr[i]);
           }else{
               System.out.print(arr[i]+",");
           }
        }
        System.out.print("}");
        System.out.println("要找的数为:");
        int num2=sc.nextInt();
        int count=0;
        for (int i = 0; i <arr.length ; i++) {
            if (arr[i]==num2){
                count++;
            }
        }
        System.out.println("出现的次数为:"+count);
    }
}
