package com.day4;

import java.util.Scanner;

public class Caogao {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入行数:");
        int line=sc.nextInt();
        for (int i = 1; i <=line; i++) {
            for (int j = line; j >i; j--) {
                System.out.print(" ");
            }
            for (int j = 1; j <=i*2-1 ; j++) {
                System.out.print("*");
            }
            System.out.println();
        }
        for (int i = line-1; i >=1 ; i--) {
            for (int j = i; j < line; j++) {
                System.out.print(" ");
            }
            for (int j =i*2-1 ; j >=1 ; j--) {
                System.out.print("*");
            }
            System.out.println();
        }


    }
}
