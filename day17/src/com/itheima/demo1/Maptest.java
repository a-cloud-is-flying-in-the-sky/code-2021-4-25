package com.itheima.demo1;

import java.util.HashMap;

public class Maptest {
    public static void main(String[] args) {
        HashMap<Student, String> hm = new HashMap<>();
        Student student1 = new Student("罗志祥",28);
        Student student5 = new Student("罗志祥",28);
        Student student2= new Student("罗玉凤",38);
        Student student3 = new Student("罗莎莎",99);
        Student student4 = new Student("罗莎莎",99);
        hm.put(student1,"杭州");
        hm.put(student2,"广州");
        hm.put(student3,"株洲");
        hm.put(student4,"株洲");
        hm.put(student5,"株洲");
        hm.forEach((Student student,String s)->{
            System.out.println(student+s);
        });
    }
}
