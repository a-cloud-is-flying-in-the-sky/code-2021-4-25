package com.itheima.demo1;

import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;

public class TestMap {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        HashMap<String,String> hm = new HashMap<>();
        hm.put("1958","巴西");
        hm.put("2000","中国");
        hm.put("1962","巴西");
        hm.put("1970","巴西");
        hm.put("1994","巴西");
        hm.put("2020","塞尔维亚");
        hm.put("2002","巴西");
        System.out.println("请输入年份:");
        String year=sc.next();
        Set<String> set = hm.keySet();
        int count=0;
        for (String s : set) {
            if (s.equals(year)){
                System.out.println(hm.get(s));
                count++;
                break;
            }
        }
        if (count==0){
            System.out.println(year+"年没有举办世界杯");
        }
        System.out.println("请输入国家:");
        String contry=sc.next();
        hm.forEach((String key,String value)->{
            if (value.equals(contry)){
                System.out.println(key);
            }
        });
    }
}
