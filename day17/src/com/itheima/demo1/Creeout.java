package com.itheima.demo1;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Creeout {
    public static void main(String[] args) {
        ArrayList<String> s = new ArrayList<>();
        s.add("张国立");
        s.add("张进");
        s.add("刘烨");
        s.add("王宝强");
        HashMap<String, String> hm = new HashMap<>();
        hm.put("aa","bb");
        hm.put("aa","bb");
        System.out.println(hm);
        List<String> list = s.stream().collect(Collectors.toList());
        Set<String> set = s.stream().collect(Collectors.toSet());
        Map<String, String> collect = s.stream().collect(Collectors.toMap((String s1) -> {
            return s1.split("a")[0];
        }, (String s1) -> {
            return s1.split("a")[1];
        }));
        System.out.println(list);
        System.out.println(set);
        System.out.println(collect);
    }
    public static void sum(int... arr){
        for (int i : arr) {
            System.out.print(i+" ");
        }
    }
}
