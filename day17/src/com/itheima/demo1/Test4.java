package com.itheima.demo1;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Test4 {
    public static void main(String[] args) {
        ArrayList<String> al = new ArrayList<>();
        al.add("李贤胜");
        al.add("李先生");
        al.add("李陷身");
        al.add("李贤");
        al.add("李胜");
        al.add("贤胜");
        ArrayList<String> al2 = new ArrayList<>();
        al2.add("李香香");
        al2.add("李白");
        al2.add("李美美");
        al2.add("李婷婷");
        al2.add("罗玉凤");
        al2.add("塞班");
        List<String> l1 = al.stream().filter(name -> name.length() == 3).limit(2).collect(Collectors.toList());
        List<String> l2 = al2.stream().filter(name -> name.startsWith("李")).skip(1).collect(Collectors.toList());
        System.out.println(l1);
        System.out.println(l2);
    }
}
