package com.itheima.Demo1;

import com.sun.xml.internal.ws.api.model.MEP;

import java.util.ArrayList;

public class Demo {
    public static void main(String[] args) {
        ArrayList<Integer> objects = new ArrayList<>();
        ArrayList<Number> objects1 = new ArrayList<>();
        ArrayList<Object> objects2 = new ArrayList<>();
        method(objects);
        method1(objects1);
        method2(objects2);
    }
    //表示传递进来的集合,可以是Number及其所有子类类型
    public static void method1(ArrayList<? extends Number> arrayList){

    }
    //表示传递进来的集合,可以的Number及其所有父类类型
    public static void method2(ArrayList<? super Number> arrayList){

    }
    public static void method(ArrayList<?> arrayList){

    }
}
