package com.itheima.Demo1;

public class Demo1 <E>{
    private E name;

    public Demo1(E name) {
        this.name = name;
    }
    public Demo1() {
    }
    public E getName() {
        return name;
    }
    public void setName(E name) {
        this.name = name;
    }
}

