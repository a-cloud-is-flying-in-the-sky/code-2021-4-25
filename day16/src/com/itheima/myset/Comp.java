package com.itheima.myset;

import java.util.Comparator;
import java.util.TreeSet;

public class Comp {
    public static void main(String[] args) {
        Student2 student1 = new Student2("liusan",20,92.2);
        Student2 student2 = new Student2("lisi",22,91.8);
        Student2 student3 = new Student2("wangwu",20,99.0);
        Student2 student4 = new Student2("sunliu",22,100);
        TreeSet<Student2> ts = new TreeSet<Student2>(new Comparator<Student2>() {
            @Override
            public int compare(Student2 o1, Student2 o2) {
               double d=o2.getScore()-o1.getScore();
               d=d==0? o1.getAge()-o2.getAge():d;
               if (d>0){
                   d=Math.ceil(d);
               }else if (d<0){
                   d=Math.floor(d);
               }
               int de=(int)d;
               return  de;
            }
        });
        ts.add(student1);
        ts.add(student2);
        ts.add(student3);
        ts.add(student4);
        System.out.println(ts);
    }
}
