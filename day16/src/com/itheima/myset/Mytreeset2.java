package com.itheima.myset;

import java.util.TreeSet;

public class Mytreeset2 {
    public static void main(String[] args) {
        Student student1 = new Student("zhangsan",15);
        Student student2 = new Student("xiaoming",16);
        Student student3 = new Student("zhaoliu",16);
        Student student4 = new Student("wangwu",16);
        TreeSet<Student> ts = new TreeSet<>();
        ts.add(student1);
        ts.add(student2);
        ts.add(student3);
        ts.add(student4);
        System.out.println(ts);
    }
}
