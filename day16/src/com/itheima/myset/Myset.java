package com.itheima.myset;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class Myset {
    public static void main(String[] args) {
        Set<String> set = new TreeSet<>();
        set.add("aaa");
        set.add("ccc");
        set.add("aaa");
        set.add("bbb");
        Iterator<String> iterator = set.iterator();
        while (iterator.hasNext()){
            String next = iterator.next();
            System.out.println(next);
        }
        System.out.println("-------------------------");
        for (String s : set) {
            System.out.println(s);
        }
    }
}
