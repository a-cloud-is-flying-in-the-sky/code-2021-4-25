package com.itheima.myset;

public class Max {
    public static void main(String[] args) {
        int[] arr = {5, 4, 2, 6, 9, 7};
        for (int i = 0; i < arr.length - 1; i++) {//比较次数
            for (int j = 0; j < arr.length - i - 1; j++) {//记录索引值
                if (arr[j] > arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }

    }
}
