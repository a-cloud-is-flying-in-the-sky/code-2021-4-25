package com.itheima.myset;

import java.util.Comparator;
import java.util.TreeSet;

public class TestEmp {
    public static void main(String[] args) {
        Employee employee1 = new Employee("小花",28,3000);
        Employee employee2 = new Employee("小心",26,4000);
        Employee employee3 = new Employee("小斯",27,4000);
        Employee employee4 = new Employee("小吖",29,7000);
        Employee employee5 = new Employee("小当",15,8000);
        TreeSet<Employee> ts = new TreeSet<>(new Comparator<Employee>() {
            @Override
            public int compare(Employee o1, Employee o2) {
                int i = o1.getSalary() - o2.getSalary();
                i=o1.getSalary()==o2.getSalary()?o1.getAge()-o2.getAge():i;
                i=o1.getAge()==o2.getAge()?o1.getName().compareTo(o2.getName()):i;
                return i;
            }
        });
        ts.add(employee1);
        ts.add(employee2);
        ts.add(employee3);
        ts.add(employee4);
        ts.add(employee5);
        System.out.println(ts);
    }
}
