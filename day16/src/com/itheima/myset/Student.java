package com.itheima.myset;

public class Student implements Comparable<Student>{
    private String name;
    private int age;
    @Override
    public int compareTo(Student o) {
        //按照对象的年龄进行排序
        int result=this.age-o.age;
        result = result == 0 ? this.name.compareTo(o.name) : result;
        //this.name.compareTo(o.name)当前对象的姓名this.name和已经存的对象的姓名o.name
        return result;
    }
    public Student() {
    }

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
