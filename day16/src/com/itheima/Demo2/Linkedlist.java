package com.itheima.Demo2;

import java.util.LinkedList;

public class Linkedlist {
    public static void main(String[] args) {
        LinkedList<String> objects = new LinkedList<>();
        objects.add("xxx");
        objects.add("aaa");
        objects.add("ccc");
        objects.add("ccc");
        objects.add("xxx");
        objects.addFirst("许超");
        System.out.println(objects);
        System.out.println(objects.getFirst());
        System.out.println(objects.get(2));
    }
}
