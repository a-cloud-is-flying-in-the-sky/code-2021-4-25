package com.itheima.Demo2;

import java.util.ArrayList;

public class Demo2 {
    public static void main(String[] args) {
        ArrayList<String> objects = new ArrayList<>();
        objects.add("aaa");
        objects.add("bbb");
        objects.add("ccc");
        objects.add("aaa");
        objects.add("bbb");
        objects.add("bbb");
        ArrayList<String> objects1 = new ArrayList<>();
        for (String object : objects) {
            if (!objects1.contains(object)){
                objects1.add(object);
            }
        }
        System.out.println(objects1);
    }
}
