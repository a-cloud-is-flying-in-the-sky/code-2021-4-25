package com.itheima.Demo2;

import java.util.ArrayList;

public class StudentTest {
    public static void main(String[] args) {
        ArrayList<Student> objects = new ArrayList<>();
        objects.add(new Student("小明",17,100));
        objects.add(new Student("小美",28,59));
        objects.add(new Student("小爱",18,66));
        objects.add(new Student("小婷",22,98));
        objects.add(new Student("小香",45,77));
        method(objects);
        sumarr(objects);
        miss(objects);
    }
    public static void method(ArrayList<Student> arrayList){
        Student maxstudent=arrayList.get(0);
        for (int i = 1; i < arrayList.size(); i++) {
            if (maxstudent.getScore()<arrayList.get(i).getScore()){
                maxstudent=arrayList.get(i);
            }
        }
        System.out.println(maxstudent);
    }
    public static void sumarr(ArrayList<Student> arrayList){
        int sum=0;
        for (int i = 0; i < arrayList.size(); i++) {
            sum+=arrayList.get(i).getScore();
        }
        System.out.println("总成绩:"+sum);
        System.out.println("平均分:"+sum/arrayList.size());
    }
    public static void miss(ArrayList<Student> arrayList){
        int count=0;
        for (int i = 0; i < arrayList.size(); i++) {
            if (arrayList.get(i).getScore()<60){
                System.out.println(arrayList.get(i));
                count++;
            }
        }
        System.out.println(count);
    }
}
