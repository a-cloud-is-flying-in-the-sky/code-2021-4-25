package com.itheima.testmain;
import com.itheima.domain.Student;
import java.util.ArrayList;
import java.util.Scanner;

public class StudentMethod {
    public ArrayList<Student> addArray(ArrayList<Student> objects){
        Student student = new Student();
        Scanner sc = new Scanner(System.in);
        System.out.println("学号:");
        student.setStudentid(sc.next());
        System.out.println("姓名:");
        student.setName(sc.next());
        System.out.println("年龄:");
        student.setAge(sc.nextInt());
        System.out.println("生日:");
        student.setBirthday(sc.next());
        objects.add(student);
        return objects;
    }//添加方法
    public ArrayList<Student> removeArray(ArrayList<Student> objects){
        Scanner sc = new Scanner(System.in);
        System.out.println("请需要删除的学号:");
        String id=sc.next();
        for (int i = 0; i < objects.size(); i++) {
            if (objects.get(i).getStudentid().equals(id)){
                objects.remove(i);
            }
        }
        return objects;
    }//删除方法
    public ArrayList<Student> setArray(ArrayList<Student> objects){
        Student stu = new Student();
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入需要修改学生的学号");
        String num=sc.next();
        for (int i = 0; i < objects.size(); i++) {
            if (objects.get(i).getStudentid().equals(num)){
                System.out.println("学号:");
                stu.setStudentid(sc.next());
                System.out.println("姓名:");
                stu.setName(sc.next());
                System.out.println("年龄:");
                stu.setAge(sc.nextInt());
                System.out.println("生日:");
                stu.setBirthday(sc.next());
                objects.set(i,stu);
            }
        }
        return objects;
    }//修改方法
    public void showArray(ArrayList<Student> objects){
        System.out.println("学号\t姓名\t年龄\t生日");
        for (int i = 0; i < objects.size(); i++) {
           Student stu= objects.get(i);
           System.out.println(stu.getStudentid()+"\t\t"+stu.getName()+"\t\t"+stu.getAge()+"\t\t"+stu.getBirthday());
        }
    }//显示方法
}
