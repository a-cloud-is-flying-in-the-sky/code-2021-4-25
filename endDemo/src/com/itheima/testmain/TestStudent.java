package com.itheima.testmain;

import com.itheima.domain.Student;

import java.util.ArrayList;
import java.util.Scanner;

public class TestStudent {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArrayList<Student> objects = new ArrayList<>();
        StudentMethod studentMethod = new StudentMethod();
        for (;;){
            System.out.println("--------欢迎来到学生管理系统--------");
            System.out.println("1.添加学生");
            System.out.println("2.删除学生");
            System.out.println("3.修改学生");
            System.out.println("4.查看学生");
            System.out.println("5.退出");
            int index=sc.nextInt();
            if (index==1){
                studentMethod.addArray(objects);
            }else if (index==2){
                studentMethod.removeArray(objects);
            }else if (index==3){
                studentMethod.setArray(objects);
            }else if (index==4){
                studentMethod.showArray(objects);
            }else if (index==5){
                System.out.println("已退出.");
                break;
            }
        }
    }
}
