package com.itheima.domain;

public class Student {
    private String name;
    private String studentid;
    private int age;
    private String birthday;

    public Student() {
    }

    public Student(String name ,String studentid, int age, String birthday) {
        this.name = name;
        this.studentid = studentid;
        this.age = age;
        this.birthday = birthday;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStudentid() {
        return studentid;
    }

    public void setStudentid(String studentid) {
        this.studentid = studentid;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }
}
