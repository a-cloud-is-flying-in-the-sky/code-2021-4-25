package com.itheima.endtest;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;

public class Test {
    public static void main(String[] args) throws IOException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Class clazz = Class.forName("com.itheima.endtest.Person");
        Constructor dc = clazz.getDeclaredConstructor();
        Object person = dc.newInstance();
        Method copy = clazz.getMethod("copy",String.class);
        Object m = copy.invoke(person,"a.txt");
        Method run = clazz.getMethod("run", String.class);
        Object runx = run.invoke(person, "流汗灵");
        Method show = clazz.getMethod("show", Date.class);
        Date date = new Date();
        Object showx = show.invoke(person,date);

    }
}
