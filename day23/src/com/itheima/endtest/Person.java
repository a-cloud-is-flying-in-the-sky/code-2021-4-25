package com.itheima.endtest;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Person {
    private String name;
    private int age;
    public void run(String name){
        System.out.println("我"+name+"是一个好人");
    }
    public void copy(String fileName) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("day23\\" + fileName));
        ArrayList<String> list = new ArrayList<>();
        String s;
        while ((s=br.readLine())!=null){
            list.add(s);
        }
        br.close();
        BufferedWriter bw = new BufferedWriter(new FileWriter("day23\\" + fileName));
        for (int i = list.size()-1; i >=0; i--) {
            bw.write(list.get(i));
            bw.newLine();
        }
        bw.flush();
        bw.close();

    }
    public void show(Date date){
        SimpleDateFormat sim = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = sim.format(date.getTime());
        System.out.println(format);
    }
    @Override
    public String toString(){
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Person() {
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }
}
