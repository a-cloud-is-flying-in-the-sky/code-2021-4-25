package com.itheima.test2;

public class Student {
    private String name;
    private int age;

    private Student(String name) {
        System.out.println("name:"+this.name);
    }

    public Student() {
        System.out.println("空参");
    }

    public Student(String name, int age) {
        System.out.println("姓名:"+name+"年龄:"+age);
        System.out.println("全参");
    }
}
