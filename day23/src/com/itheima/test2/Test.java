package com.itheima.test2;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class Test {
    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchFieldException {
        Class clazz = Class.forName("com.itheima.test2.Student");
        Field name = clazz.getDeclaredField("name");
        System.out.println(name);
        Constructor constructor1 = clazz.getConstructor(String.class,int.class);
        //创建构造方法对象相当于调用构造方法
        Student student = (Student) constructor1.newInstance("xc", 26);



    }
}
