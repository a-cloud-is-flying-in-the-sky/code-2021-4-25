package com.itheima.test1;

public class Demo2 {
    public static void main(String[] args) throws ClassNotFoundException {
        //源代码阶段(没有加载进内存)
        Class c1 = Class.forName("com.itheima.test1.Demo2");
        //Class对象阶段(已加载入内存)
        Class c2 = Demo2.class;
        Demo2 demo2 = new Demo2();
        //运行是阶段
        Class c3 = demo2.getClass();
    }
}
