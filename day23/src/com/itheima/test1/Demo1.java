package com.itheima.test1;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

public class Demo1 {
    public static void main(String[] args) throws IOException {
        ClassLoader sc =ClassLoader.getSystemClassLoader();
        InputStream resourceAsStream = sc.getResourceAsStream("pro.properties");
        Properties pp = new Properties();
        pp.load(resourceAsStream);
        System.out.println(pp);
        resourceAsStream.close();
        System.out.println("---------------------");
        URL resource = sc.getResource("pro.properties");
        String path = resource.getPath();
        System.out.println(path);
    }
}
