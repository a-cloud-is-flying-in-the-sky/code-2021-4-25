package com.itheima.test3;

public class Student {
    private String name;
    private int age;
    public String arr;

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", arr='" + arr + '\'' +
                '}';
    }
    public void method1(){
        System.out.println("public方法1");
    }
    private void method2(int a){
        this.age=a;
        System.out.println("私有方法2,参数:"+age);
    }
    private String method3(String s){
        System.out.println("私有方法3,有返回值");
        return s;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getArr() {
        return arr;
    }

    public void setArr(String arr) {
        this.arr = arr;
    }

    public Student() {
        System.out.println("空参public");
    }

    private Student(String name, int age, String arr) {
        System.out.println(name+"今年"+age+"岁,住在"+arr);
        System.out.println("private");
    }

    public Student(String name) {
        System.out.println(name);
        System.out.println("public");
    }

}
