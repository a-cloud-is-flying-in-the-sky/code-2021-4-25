package com.itheima.test3;

import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Properties;

public class Test {
    public static void main(String[] args) throws   Exception{
        InputStream rs = ClassLoader.getSystemClassLoader().getResourceAsStream("pro.properties");
        Properties properties = new Properties();
        properties.load(rs);
        Class clazz = Class.forName(properties.getProperty("fieldPro"));
        rs.close();
        Constructor dec = clazz.getDeclaredConstructor();
        Student student = (Student) dec.newInstance();
        Field dlf = clazz.getDeclaredField(properties.getProperty("fieldName"));
        dlf.setAccessible(true);
        dlf.set(student,"许超");
        System.out.println(dlf.get(student));
        /*Class clazz = Class.forName("com.itheima.test3.Student");
        Student student = (Student) clazz.newInstance();//创建对象
        Constructor declaredConstructor = clazz.getDeclaredConstructor();
        Student student1 = (Student) declaredConstructor.newInstance();
        Method method1 = clazz.getDeclaredMethod("method1");
        Method method3 = clazz.getDeclaredMethod("method3", String.class);
        method3.setAccessible(true);
        Method method2 = clazz.getDeclaredMethod("method2", int.class);
        method2.setAccessible(true);
        Student invoke1 = (Student) method1.invoke(student);
        Object invoke2 = method2.invoke(student,23);//一般不转型
        Object invoke3 =  method3.invoke(student1,"徐擦后");
        System.out.println(invoke2);
        System.out.println(invoke3);*/

        /*Field name = clazz.getDeclaredField("name");
        Field[] declaredFields = clazz.getDeclaredFields();
        for (Field declaredField : declaredFields) {
            declaredField.setAccessible(true);
            System.out.println(declaredField.get(student));
        }*/
        /*Method method1 = clazz.getDeclaredMethod("method1");
        Student invoke = (Student) method1.invoke(student);//接收方法返回值,没有则为null
        Method method3 = clazz.getDeclaredMethod("method3", String.class);
        method3.setAccessible(true);
        Object xc = method3.invoke(student, "许超");
        System.out.println(method1);*/
       // Constructor declaredConstructor = clazz.getDeclaredConstructor(String.class,int.class,String.class);
        /*Constructor declaredConstructor = clazz.getDeclaredConstructor(String.class,int.class,String.class);
        declaredConstructor.setAccessible(true);
        Student con = (Student) declaredConstructor.newInstance("续钞傲",28,"商务职业学院");
        System.out.println(con);*/
    }
}
