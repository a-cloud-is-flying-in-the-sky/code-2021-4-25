package com.itheima.demo;

public class Zilei2 extends Fulei{
    private String tchid;

    public Zilei2(String name, int age, String sex, String tchid) {
        super(name, age, sex);
        this.tchid = tchid;
    }

    public void tch(){
        System.out.println("教书");
    }

    public String getTchid() {
        return tchid;
    }

    public void setTchid(String tchid) {
        this.tchid = tchid;
    }
}
