package com.itheima.demo2;

public class Frog extends Animal implements swimming{
    @Override
    public void eat() {
        System.out.println("青蛙吃虫子");
    }

    @Override
    public void moving() {
        System.out.println("青蛙会蛙泳");
    }
}
