package com.itheima.demo2;

public class Test {
    public static void main(String[] args) {
        Breed breed = new Breed();
        breed.breed(new Dog());
        breed.breed(new Frog());
        breed.breed(new Sheep());
    }
}
