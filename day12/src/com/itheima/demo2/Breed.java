package com.itheima.demo2;

public class Breed extends Animal{
    @Override
    public void eat() {
        System.out.println("吃饭");
    }
    public void breed(Animal animal){
        if (animal instanceof Dog){
            animal.drink();
            animal.eat();
            Dog dog=(Dog)animal;
            dog.moving();
        }else if (animal instanceof Sheep){
            animal.drink();
            animal.eat();
        }else if (animal instanceof Frog){
            animal.drink();
            animal.eat();
            Frog frog=(Frog)animal;
            frog.moving();
        }else{
            System.out.println("查无此类");
        }


    }
}
