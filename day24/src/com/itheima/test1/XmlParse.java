package com.itheima.test1;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class XmlParse {
    public static void main(String[] args) throws DocumentException {
        SAXReader saxReader = new SAXReader();
        Document document = saxReader.read(new File("day24\\xml\\student.xml"));
        Element rootElement = document.getRootElement();
        List<Element> listElement = rootElement.elements();
        ArrayList<Student> list = new ArrayList<>();
        for (Element element : listElement) {
            Attribute att = element.attribute("id");
            String id = att.getValue();
            Element nameElement = element.element("name");
            String name = nameElement.getText();
            Element ageElement = element.element("age");
            String age = ageElement.getText();
            Student s = new Student(name,Integer.parseInt(age));
            list.add(s);
        }
        System.out.println(list);
    }
}
