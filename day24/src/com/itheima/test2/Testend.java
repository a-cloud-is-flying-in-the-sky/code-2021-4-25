package com.itheima.test2;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class Testend {
    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Class clazz = Class.forName("com.itheima.test2.Method");
        Constructor constructor = clazz.getDeclaredConstructor();
        Method method = (Method) constructor.newInstance();
        java.lang.reflect.Method[] methods = clazz.getMethods();
        for (java.lang.reflect.Method method1 : methods) {
            if (method1.isAnnotationPresent(Test.class)){
                Object invoke = method1.invoke(method);
            }
        }
    }
}
