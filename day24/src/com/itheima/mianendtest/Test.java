package com.itheima.mianendtest;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
public class Test {
    public static void main(String[] args) throws IOException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        //创建类加载器对象
        ClassLoader scl = ClassLoader.getSystemClassLoader();
        //加载pro配置文件
        InputStream rs = scl.getResourceAsStream("pro.properties");
        Properties pro = new Properties();
        pro.load(rs);
        String userpath = pro.getProperty("UserName");
        Class<?> clazz = Class.forName(userpath);
        Constructor<?> dc = clazz.getDeclaredConstructor();
        dc.setAccessible(true);
        Object user = dc.newInstance();
        Method[] ms = clazz.getDeclaredMethods();
        BufferedWriter bw = new BufferedWriter(new FileWriter("day24\\bug.txt",true));
        int count=0;
        for (Method m : ms) {
            if (m.isAnnotationPresent(MyTest.class)){
                try{
                    m.setAccessible(true);
                    m.invoke(user);
                }catch (Exception o){
                    String name = m.getName();
                    bw.write(name+"方法发生异常");
                    bw.newLine();
                    Date date = new Date();
                    SimpleDateFormat sim = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss:SS");
                    String time = sim.format(date);
                    bw.write("异常时间为:"+time);
                    bw.newLine();
                    String simpleName = o.getCause().getClass().getSimpleName();
                    bw.write("异常名称为:"+simpleName);
                    bw.newLine();
                    String cause = o.getCause().getMessage();
                    bw.write("异常的原因为:"+cause);
                    bw.newLine();
                    bw.flush();
                    count++;
                }
            }
        }
        bw.write("------本次测试一共出现了"+count+"次异常------");
        bw.newLine();
        bw.close();

    }
}
