package com.itheima.miankuangjia;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.HashMap;

public class BeanFactory2 {
    private BeanFactory2(){}
    private static HashMap<Object,Object> hm=new HashMap<>();
    static {
        try {
            ClassLoader classLoader = BeanFactory2.class.getClassLoader();
            String path = classLoader.getResource("element.xml").getPath();
            Document parse = Jsoup.parse(new File(path), "UTF-8");
            Elements bean = parse.getElementsByTag("bean");
            for (Element element : bean) {
                String idv = element.attr("id");
                String classv = element.attr("class");
                Class<?> aClass = Class.forName(classv);
                Constructor<?> con = aClass.getDeclaredConstructor();
                con.setAccessible(true);
                Object obj = con.newInstance();
                Elements property = element.getElementsByTag("property");
                for (Element element1 : property) {
                    String namev = element1.attr("name");
                    String valuev = element1.attr("value");
                    Field field = aClass.getDeclaredField(namev);
                    field.setAccessible(true);
                    String typestring = field.getType().toString();
                    if (typestring.contains("int")){
                        field.set(obj,Integer.parseInt(valuev));
                    }else{
                        field.set(obj,valuev);
                }
                }
                hm.put(idv,obj);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static Object getBean(String s){
        return hm.get(s);
    }
}
