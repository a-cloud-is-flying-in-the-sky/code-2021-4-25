package com.itheima.test1;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Log4jtest {
    private static final Logger LOGGER= LoggerFactory.getLogger(Log4jtest.class);

    public static void main(String[] args) {
        LOGGER.debug("DEBUG");
        LOGGER.info("info");
        LOGGER.warn("warn");
        LOGGER.error("error");
    }
}
