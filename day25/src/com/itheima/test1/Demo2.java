package com.itheima.test1;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class Demo2 {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(520);
        list.add(1314);
        Class<? extends ArrayList> listClass = list.getClass();
        Method add = listClass.getDeclaredMethod("add", Object.class);
        add.invoke(list,"xc");
        System.out.println(list);

    }
}
