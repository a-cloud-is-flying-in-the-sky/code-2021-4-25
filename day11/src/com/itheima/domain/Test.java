package com.itheima.domain;

import com.sun.org.apache.bcel.internal.generic.NEW;

public class Test {
    public static void main(String[] args) {
        AnimalOperator anm = new AnimalOperator();
        Dog dog = new Dog();
        Cat cat = new Cat();
        anm.useAnimal(dog);
        anm.useAnimal(cat);
    }
}
