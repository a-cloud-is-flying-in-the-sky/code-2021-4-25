package com.itheima.demo1;

import java.util.ArrayList;

public class Dianqi {
    private String pinpai;
    private String xinghao;
    private String color;
    private int price;

    public Dianqi() {
    }

    public Dianqi(String pinpai, String xinghao, String color, int price) {
        this.pinpai = pinpai;
        this.xinghao = xinghao;
        this.color = color;
        this.price = price;
    }

    public String getPinpai() {
        return pinpai;
    }

    public void setPinpai(String pinpai) {
        this.pinpai = pinpai;
    }

    public String getXinghao() {
        return xinghao;
    }

    public void setXinghao(String xinghao) {
        this.xinghao = xinghao;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
