package com.itheima.demo1;

public class Freezer extends Dianqi{
    public Freezer() {
    }

    public Freezer(String pinpai, String xinghao, String color, int price) {
        super(pinpai, xinghao, color, price);
    }
    public void work(){
        System.out.println(super.getPinpai()+"品牌,型号为"+super.getXinghao()+"颜色"+super.getColor()+"售价为:"+super.getPrice());
    }
    public void storage(){
        System.out.println("储存实物....");
    }
}
