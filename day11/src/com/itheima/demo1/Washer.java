package com.itheima.demo1;

public class Washer extends Dianqi{
    public Washer() {
    }

    public Washer(String pinpai, String xinghao, String color, int price) {
        super(pinpai, xinghao, color, price);
    }
    public void work(){
        System.out.println(super.getPinpai()+"品牌,型号为"+super.getXinghao()+"颜色"+super.getColor()+"售价为:"+super.getPrice());
    }
    public void doSomeWashing(){
        System.out.println("洗衣服");
    }
}
