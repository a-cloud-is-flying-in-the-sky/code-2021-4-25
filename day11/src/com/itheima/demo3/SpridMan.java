package com.itheima.demo3;

public abstract class SpridMan {
    private String id;
    private String name;
    private int age;
    public SpridMan(String id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public abstract void work();
    public SpridMan() {
    }

    public final void sendMessage(){
        System.out.println("通知:");
        work();
        System.out.println("\t\t必胜环球科技有限公司");
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
