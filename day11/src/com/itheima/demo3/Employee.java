package com.itheima.demo3;

public class Employee extends SpridMan{
    private String getM;

    public Employee() {
    }
    public Employee(String getM) {
        this.getM=getM;
    }
    public Employee(String id, String name, int age) {
        super(id, name, age);
    }

    public String getGetM() {
        return getM;
    }

    public void setGetM(String getM) {
        this.getM = getM;
    }


    @Override
    public void work(){
        System.out.println(this.getGetM());
    }
}
