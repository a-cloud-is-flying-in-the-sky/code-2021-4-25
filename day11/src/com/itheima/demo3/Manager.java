package com.itheima.demo3;

public class Manager extends SpridMan{
    private String getcontent;
    private int salary;

    public Manager(String id, String name, int age, int salary) {
        super(id, name, age);
        this.salary = salary;
    }
    @Override
    public void work(){
        System.out.println(this.getcontent);
    }

    public Manager(String getcontent) {
        this.getcontent=getcontent;
    }
}
