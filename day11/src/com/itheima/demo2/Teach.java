package com.itheima.demo2;

public class Teach extends Workman{
    private String teachid;

    @Override
    public void eat() {
        System.out.println("吃工作餐");
    }

    public Teach() {
    }

    public Teach(String name, String sex, int age, String teachid) {
        super(name, sex, age);
        this.teachid = teachid;
    }

    public String getTeachid() {
        return teachid;
    }

    public void setTeachid(String teachid) {
        this.teachid = teachid;
    }

}
