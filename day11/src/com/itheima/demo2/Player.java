package com.itheima.demo2;

public class Player extends Workman{
    private String stuid;
    private String class1;

    @Override
    public void eat() {
        System.out.println("吃营养餐");
    }

    public Player(String name, String sex, int age, String stuid, String class1) {
        super(name, sex, age);
        this.stuid = stuid;
        this.class1 = class1;
    }

    public Player(String name, String sex, int age) {
        super(name, sex, age);
    }

    public Player(String stuid, String class1) {
        this.stuid = stuid;
        this.class1 = class1;
    }

    public Player() {
    }

    public String getStuid() {
        return stuid;
    }

    public void setStuid(String stuid) {
        this.stuid = stuid;
    }

    public String getClass1() {
        return class1;
    }

    public void setClass1(String class1) {
        this.class1 = class1;
    }
}
