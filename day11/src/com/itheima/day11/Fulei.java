package com.itheima.day11;

public class Fulei {
    private String name;
    private int age;
    private int salery;
    public void work(){

    }

    public Fulei() {
    }

    public Fulei(String name, int age, int salery) {
        this.name = name;
        this.age = age;
        this.salery = salery;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getSalery() {
        return salery;
    }

    public void setSalery(int salery) {
        this.salery = salery;
    }
}
