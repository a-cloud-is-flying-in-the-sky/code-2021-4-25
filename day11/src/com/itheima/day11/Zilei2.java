package com.itheima.day11;

public class Zilei2 extends Fulei{
    private int sale;


    public Zilei2(String name, int age, int salery, int sale) {
        super(name, age, salery);
        this.sale = sale;
    }
    @Override
    public void work() {
        super.work();
        System.out.println(super.getAge()+"岁的"+super.getName()+"在敲代码,工资是:"+super.getSalery()+"奖金是"+this.sale);
    }

    public int getSale() {
        return sale;
    }

    public void setSale(int sale) {
        this.sale = sale;
    }
}
