package com.itheima.Test3;

public class Filam implements Runnable{
    @Override
    public void run() {
        while (true){
            synchronized (Demo.lock){
                try {
                    Thread.sleep(30);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (Demo.count==0)
                    break;
                Demo.count--;
                System.out.println(Thread.currentThread().getName()+"卖出,还剩"+Demo.count);
            }
        }
    }
}
