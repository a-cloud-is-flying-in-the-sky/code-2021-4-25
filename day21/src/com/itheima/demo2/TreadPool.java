package com.itheima.demo2;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class TreadPool{
    public static void main(String[] args) {
        ThreadPoolExecutor tp = new ThreadPoolExecutor(2,5,2, TimeUnit.SECONDS,new ArrayBlockingQueue<>(5), Executors.defaultThreadFactory(),new ThreadPoolExecutor.AbortPolicy());
        for (int i = 0; i < 10; i++) {
            tp.submit(()->{
                System.out.println(Thread.currentThread().getName()+"执行");
            });
        }
        tp.shutdown();

    }
}
