package com.itheima.demo3;

import java.util.concurrent.CountDownLatch;

public class Mythread1 extends Thread {
    private CountDownLatch countDownLatch;
    public Mythread1(CountDownLatch countDownLatch) {
        this.countDownLatch=countDownLatch;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println("第"+i+"次");
        }
        countDownLatch.countDown();
    }
}
