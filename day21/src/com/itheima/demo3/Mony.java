package com.itheima.demo3;

import java.util.concurrent.CountDownLatch;

public class Mony extends Thread{
    private CountDownLatch countDownLatch;
    public Mony(CountDownLatch countDownLatch) {
        this.countDownLatch=countDownLatch;
    }

    @Override
    public void run() {
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("执行了");
    }
}
