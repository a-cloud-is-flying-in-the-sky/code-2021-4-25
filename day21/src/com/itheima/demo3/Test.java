package com.itheima.demo3;

import java.util.concurrent.CountDownLatch;

public class Test {
    public static void main(String[] args) {
        CountDownLatch countDownLatch = new CountDownLatch(2);
        Mony mony = new Mony(countDownLatch);
        mony.start();
        Mythread1 m1 = new Mythread1(countDownLatch);
        m1.start();
        Mythread2 m2 = new Mythread2(countDownLatch);
        m2.start();
    }
}
