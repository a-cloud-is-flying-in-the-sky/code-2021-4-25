package com.itheima.Test5;
/*第五题：(有难度! 可选做!!!)
        某公司组织年会,会议入场时有两个入口,在入场时每位员工都能获取一张双色球彩票,
        假设公司有100个员工,利用多线程模拟年会入场过程,
        并分别统计每个入口入场的人数,以及每个员工拿到的彩票的号码。线程运行后打印格式如下：
        编号为: 1 的员工 从后门 入场! 拿到的双色球彩票号码是: [17, 24, 29, 30, 31, 32, 7]
        编号为: 2 的员工 从后门 入场! 拿到的双色球彩票号码是: [6, 11, 14, 22, 29, 32, 15]
        //.....
        从后门入场的员工总共: xx 位员工
        从前门入场的员工总共: XX 位员工

        双色球组成:
        红球:6个 范围:1~33  不可以重复
        蓝球:1个 范围:1~16  蓝球可以和红球重复
        双色球是可以同时开出多注特等奖*/

public class Test {
    public static int count=100;
    static Object lock=new Object();
    public static void main(String[] args) {
        Qiang qiang = new Qiang();
        Thread t1 = new Thread(qiang,"前门");
        Thread t2 = new Thread(qiang,"后门");
        t1.start();
        t2.start();

    }
}
