package com.itheima.demo1;

import java.util.concurrent.ArrayBlockingQueue;

public class Demo1 {
    public static void main(String[] args) {
        ArrayBlockingQueue arrayBlockingQueue = new ArrayBlockingQueue<String>(1);
        Foodie foodie = new Foodie(arrayBlockingQueue);
        Cooker cooker = new Cooker(arrayBlockingQueue);
        foodie.start();
        cooker.start();
    }
}
