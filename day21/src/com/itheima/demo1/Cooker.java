package com.itheima.demo1;

import java.util.concurrent.ArrayBlockingQueue;

public class Cooker extends Thread{
    ArrayBlockingQueue queue;
    public Cooker(ArrayBlockingQueue arrayBlockingQueue) {
        this.queue=arrayBlockingQueue;
    }

    @Override
    public void run() {
        while (true){
            try {
                    Thread.sleep(10);
                    queue.take();
                    System.out.println("吃货吃了一个包子");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
