package com.itheima.demo1;

import java.util.concurrent.ArrayBlockingQueue;

public class Foodie extends  Thread{
    ArrayBlockingQueue queue;
    public Foodie(ArrayBlockingQueue arrayBlockingQueue) {
        this.queue=arrayBlockingQueue;
    }

    @Override
    public void run() {
        while (true){
            try {
                    Thread.sleep(10);
                    queue.put("包子");
                    System.out.println("厨师做了一个包子");
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }
}
