package com.itheima.Test4;

import java.util.ArrayList;
import java.util.Random;

public class MyRunable implements Runnable{
    private ArrayList<Integer> list;
    public MyRunable(ArrayList<Integer> list) {
        this.list=list;
    }

    @Override
    public void run() {
        int count=0;
        Random r = new Random();
        while (true){
            synchronized (Test.lock){
                if (list.size()==0)break;
                int i= r.nextInt(list.size());
                System.out.println(Thread.currentThread().getName()+"产生了一个"+list.get(i)+"大奖");
                list.remove(i);
                count++;
            }
        }
        System.out.println(Thread.currentThread().getName()+"抽取了"+count+"次");
    }
}
