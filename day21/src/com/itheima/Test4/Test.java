package com.itheima.Test4;

import java.util.ArrayList;

public class Test {
    static Object lock=new Object();
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        int[] arr = new int[]{10,5,20,50,100,200,500,800,2,80,300,700};
        for (int i : arr) {
            list.add(i);
        }
        MyRunable m1 = new MyRunable(list);
        Thread t1 = new Thread(m1,"抽奖箱1");
        Thread t2 = new Thread(m1,"抽奖箱2");
        t1.start();
        t2.start();
    }
}
