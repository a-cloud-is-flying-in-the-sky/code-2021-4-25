package com.itheima.Test1;

public class Wangdian extends Thread{
    private int sum=0;
    @Override
    public void run(){
        while (true){
            synchronized (Demo.lock){
                if (Demo.count<=0){
                    break;
                }
                Demo.count--;
                this.sum++;
                System.out.println("网店卖出一个,还剩"+Demo.count+"个");
            }
        }
        System.out.println("网店卖了"+sum);
    }
}
