package com.itheima.Test6;

import java.util.concurrent.*;

public class Demo1 {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newCachedThreadPool();
        ExecutorService executorService1 = Executors.newFixedThreadPool(10);
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(2,5,2, TimeUnit.SECONDS,new ArrayBlockingQueue<>(5),Executors.defaultThreadFactory(),new ThreadPoolExecutor.DiscardOldestPolicy());
        executorService.submit(()->{
            System.out.println(Thread.currentThread().getName()+"123");
        });
        executorService.submit(()->{
            System.out.println(Thread.currentThread().getName()+"123");
        });
    }
}
