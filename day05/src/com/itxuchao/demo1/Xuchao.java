package com.itxuchao.demo1;

public class Xuchao {
    public static void main(String[] args) {
        isMax(10,20);
        isMax((byte)10,(byte)20);
        isMax(10L,20L);
    }
    public static int isMax(int a,int b){
        System.out.println("int");
        return a+b;
    }
    public static byte isMax(byte a,byte b){
        System.out.println("byte");
        return (byte)(a+b);
    }
    public static long isMax(long a,long b){
        System.out.println("long");
        return (long)(a+b);
    }
}
