import java.io.File;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        int count=0;
        Scanner sc = new Scanner(System.in);
        System.out.println("输入一个文件夹路径:");
        String name = sc.next();
        File file = new File(name);
        count=method(file,count);
        System.out.println(name+"文件夹下一共有"+count+"个文件");
    }
    private static int method(File file ,int count) {
        File[] files = file.listFiles();
        if (files!=null){
            for (File file1 : files) {
                if (file1.isFile()){
                    count++;
                }else{
                  count=method(file1,count);
                }
            }
        }
        return count;
    }
}
