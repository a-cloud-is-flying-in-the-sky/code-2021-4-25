import java.io.File;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Scanner;

public class Test3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入一个文件夹路径:");
        String s = sc.next();
        File file = new File(s);
        while (!file.exists()) {
            System.out.println("路径不正确,请重新输入一个文件夹路径:");
            s = sc.next();
            file = new File(s);
            if (file.exists()) {
                break;
            }
        }
        show(file);
    }

    private static void show(File file) {
        File[] files = file.listFiles();
        if (files != null) {
            for (File file1 : files) {
                if (file1.isFile()) {
                    String name = file1.getName();
                    String[] split = name.split("\\.");
                    if (split.length == 2 && split[1].equals("txt")) {
                        System.out.println(name);
                    }
                } else if (file1.isDirectory()) {
                    show(file1);
                }
            }
        }
    }
}
