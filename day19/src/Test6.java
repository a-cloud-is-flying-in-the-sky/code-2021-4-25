import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

public class Test6 {
    public static void main(String[] args) throws IOException {
        String s="sda";
        boolean b=s.startsWith("s");
        System.out.println(b);
        Properties pt = new Properties();
        FileReader fileReader = new FileReader("day19\\pro.properties");
        pt.load(fileReader);//使用load方法将指定的pro文件存入集合中
        fileReader.close();
        System.out.println(pt);
        pt.put("name","xc");
        pt.put("哎哎哎","睡衣守护者");
        pt.put("www","爱搜都");
        FileWriter fileWriter = new FileWriter("day19\\pro.properties");
        pt.store(fileWriter,"null");
        fileWriter.close();
    }
}
