import java.io.*;
public class Workend1 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("day19\\bbb.txt"));
        String s = br.readLine();
        br.close();
        String[] s1 = s.split(" ");
        int[] ints = new int[s1.length];
        for (int i = 0; i < s1.length; i++) {
            ints[i]=Integer.parseInt(s1[i]);
        }
        for (int i = 0; i < ints.length-1; i++) {
            for (int j = 0; j <ints.length-i-1; j++) {
                if (ints[j] > ints[j+1]) {
                    int temp=ints[j];
                    ints[j]=ints[j+1];
                    ints[j+1]=temp;
                }
            }
        }
        BufferedWriter bw = new BufferedWriter(new FileWriter("day19\\bbb.txt"));
        for (int i = 0; i < ints.length; i++) {
            bw.write(ints[i]+" ");
        }
        bw.close();
    }
}
