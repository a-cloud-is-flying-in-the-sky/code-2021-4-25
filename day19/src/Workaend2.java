import java.io.*;
import java.util.ArrayList;

public class Workaend2 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("day19\\ccc.txt"));
        ArrayList<String> list = new ArrayList<>();
        String s;
        while ((s=br.readLine())!=null){
            list.add(s);
        }
        BufferedWriter bw = new BufferedWriter(new FileWriter("day19\\ccc.txt"));
        for (int i = list.size()-1; i >=0; i--) {
            bw.write(list.get(i)+"\r");
        }
        bw.close();
    }
}
