import java.io.Serializable;
import java.util.ArrayList;

public class Student implements Serializable, Comparable<Student>{
    private static final long serialVersionUID = 1L;
    private String username;
    private String password;
    public Student() {
    }
    public Student(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Student{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    @Override
    public int compareTo(Student o) {
        return o.getPassword().compareTo(this.password);
    }
}
