import java.io.*;
import java.util.ArrayList;

public class Workend3 {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        ArrayList<Student> list = new ArrayList<>();
        list.add(new Student("撒读","萨迪"));
        list.add(new Student("阿萨德","少年"));
        list.add(new Student("爱搜都","奥德赛"));
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("day19\\ddd.txt"));
        oos.writeObject(list);
        oos.close();
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("day19\\ddd.txt"));
        ArrayList<Student> o = (ArrayList<Student>)ois.readObject();
        System.out.println(o);
        ois.close();
    }
}
