import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.TreeSet;

public class Objectstream {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Student s1 = new Student("许超","16161");
        Student s2 = new Student("许超啊啊","24655");
        Student s3 = new Student("许超哦哦","3515");
        TreeSet<Student> set = new TreeSet<>();
        set.add(s1);
        set.add(s2);
        set.add(s3);
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("day19\\aaa.txt"));
        oos.writeObject(set);
        oos.close();
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("day19\\aaa.txt"));
        TreeSet<Student> o = (TreeSet<Student>) ois.readObject();
        System.out.println(o);
        ois.close();
    }
}
