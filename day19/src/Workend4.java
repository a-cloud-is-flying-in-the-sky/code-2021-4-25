import java.io.*;
import java.util.Properties;

public class Workend4 {
    public static void main(String[] args) throws IOException {
        Properties pt = new Properties();
        String s;
        pt.load(new FileReader("day19\\score.txt"));
        System.out.println(pt);
        if (pt.containsKey("list")){
            pt.put("list","100");
        }
        pt.store(new FileWriter("day19\\score.txt"),null);
    }
}
