package com.itheima.demo;

import com.sun.org.apache.bcel.internal.generic.NEW;

import java.util.Scanner;

public class Zhuoye06 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] arr = new int[6];
        for (int i = 0; i < arr.length; i++) {
            System.out.println("请输入第"+(i+1)+"个数组元素");
            arr[i]=sc.nextInt();
        }
        System.out.print("初始数组为:");
        forArr(arr);
        arrFanzhuan(arr);
        arrJishu(arr);
        arrStrEnd(arr);
    }
    public static void arrFanzhuan(int[] arr){
        int[] arr2 =arr;
        for (int i = 0; i < arr2.length / 2; i++) {
            int temp=arr2[i];
            arr2[i]=arr2[arr2.length-1-i];
            arr2[arr2.length-1-i]=temp;
        }
        System.out.print("反转数组为:");
        forArr(arr2);
    }//2
    public static void arrJishu(int[] arr){
        int[] arr3 = arr;
        for (int start = 0; start < arr3.length;start++) {
            if (start+2>arr3.length-1){
                break;
            }
            if (start%2!=0){
                int temp=arr3[start];
                arr3[start]=arr3[start+2];
                arr3[start+2]=temp;
            }
        }
        System.out.print("反转奇数索引数组:");
        forArr(arr3);
    }//3
    public static void arrStrEnd(int[] arr){
        int[] arr4 =arr;
        int temp;
            if (arr4.length%2==0){
                temp=arr4[1];
                arr4[1]=arr4[arr4.length-1];
                arr4[arr4.length-1]=temp;
            }else{
                temp=arr4[1];
                arr4[1]=arr4[arr4.length-2];
                arr4[arr4.length-2]=temp;
            }
        System.out.print("最后的数组为:");
            forArr(arr);
    }//4
    public static void forArr(int[] arr) {
        System.out.print("[");
        for (int i = 0; i < arr.length; i++) {
            if (i == arr.length - 1) {
                System.out.print(arr[i]);
            } else {
                System.out.print(arr[i] + ",");
            }
        }
        System.out.println("]");
    }//遍历输出一个数组
}
