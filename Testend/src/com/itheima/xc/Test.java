package com.itheima.xc;

import java.util.Random;

public class Test {
    public static void main(String[] args) {
        /*Xc1.Xc2 xc=new Xc1.Xc2();//静态类非静态方法调用

        Xc1 xc1=new Xc1();
        Xc1.Xc2.show();//静态类静态方法调用
        xc.method();*/
        Xc1 xc1=new Xc1();
       method(()->{
           Random r=new Random();
           System.out.println("啦啦啦");
           return r.nextInt(10);
       });
    }
    public static void method(Inter it){
        it.eat();
    }
}
interface Inter{
    int eat();
                }
class Xc1{
     private class Xc2{
        public void method(){
            System.out.println("method");
        }
        public  void show(){
            System.out.println("show");
        }
    }
}
