package com.itheima.xc;

public class JavaBean {
    public static void main(String[] args) {
        Book book1 = new Book("book001","三国演义",80);
        Book book2 = new Book("book002","水浒传",80);
        Book book3 = new Book("book003","故事会",5);
        book1.show();
        book2.show();
        book3.show();
    }
}
