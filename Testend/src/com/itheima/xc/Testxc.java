package com.itheima.xc;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Objects;

public class Testxc {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(520);
        list.add(1314);
        Class<? extends ArrayList> clazz = list.getClass();
        Method add = clazz.getMethod("add",Object.class);
        add.setAccessible(true);
        Object invoke = add.invoke(list, "字符串");
        System.out.println(list);
    }
}
