package com.itheima.test1;

import java.io.IOException;
import java.net.*;

public class SentDemo1 {
    public static void main(String[] args) throws IOException {
        DatagramSocket ds = new DatagramSocket();
        String s = "lalala";
        byte[] bytes = s.getBytes();
        InetAddress byName = InetAddress.getByName("224.0.1.0");
        DatagramPacket dp = new DatagramPacket(bytes,bytes.length,byName,1028 );
        ds.send(dp);
        ds.close();
    }
}
