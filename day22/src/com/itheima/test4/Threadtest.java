package com.itheima.test4;
import java.io.*;
import java.net.Socket;
import java.util.UUID;
public class Threadtest implements Runnable{
    private Socket accept;
    public Threadtest(Socket accept) {
        this.accept=accept;
    }
    @Override
    public void run() {
        BufferedInputStream bis=null;
        BufferedOutputStream bos=null;
        try {
            bis = new BufferedInputStream(accept.getInputStream());
            bos = new BufferedOutputStream(new FileOutputStream("day22\\" + UUID.randomUUID().toString() + ".txt"));
            int b;
            while ((b=bis.read())!=-1){
                bos.write(b);
            }
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(accept.getOutputStream()));
            bw.write("操作完成");
            bw.flush();
            accept.shutdownOutput();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bis!=null){
                try {
                    bis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (bos!=null){
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }
}
