package com.itheima.test4;
import java.io.*;
import java.net.Socket;
public class CilenTest {
    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("127.0.0.1",12347);
        while (true){
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream("day22\\a.txt"));
            BufferedOutputStream bos = new BufferedOutputStream(socket.getOutputStream());
            socket.close();
            bis.close();
            bos.close();
            int b;
            while ((b=bis.read())!=-1){
                bos.write(b);
            }
            bos.flush();
            socket.shutdownOutput();
        }
        /*BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        String s;
        br.close();*/
        /*while ((s=br.readLine())!=null){
            System.out.println(s);
        }*/

    }
}
