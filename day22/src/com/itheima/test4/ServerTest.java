package com.itheima.test4;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ServerTest {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(12347);
        ThreadPoolExecutor tpe = new ThreadPoolExecutor(3, 8, 2, TimeUnit.SECONDS, new ArrayBlockingQueue<>(10), Executors.defaultThreadFactory(), new ThreadPoolExecutor.AbortPolicy());
        while (true){
            Socket accept = serverSocket.accept();
            Threadtest threadtest = new Threadtest(accept);
            tpe.submit(threadtest);
        }
    }
}
