package com.itheima.tcptest;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerDemo {
    public static void main(String[] args) throws IOException {
        ServerSocket ss = new ServerSocket(10000);
        InputStream is = ss.accept().getInputStream();
        int b;
        while ((b=is.read())!=-1){
            System.out.println((char) b);
        }
        is.close();
        ss.close();
    }
}
