package com.itheima.test2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

public class ClientTest {
    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("127.0.0.1",12349);
        OutputStream os = socket.getOutputStream();
        byte[] bytes = "helow".getBytes();
        os.write(bytes);
        socket.shutdownOutput();
        BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        String s;
        while ((s=br.readLine())!=null){
            System.out.println(s);
        }
        br.close();
        os.close();
        socket.close();

    }
}
