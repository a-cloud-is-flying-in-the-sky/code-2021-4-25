package com.test3;

import java.io.*;
import java.net.Socket;

public class ClienTest {
    public static void main(String[] args) throws IOException {
        //1.创建客户端套接字对象,指定ip和端口号
    Socket socket = new Socket("127.0.0.1",12567);
    //2.将本地文件数据读取到内存
    BufferedInputStream bis = new BufferedInputStream(new FileInputStream("day22\\a.txt"));
    //3.将读取到的数据通过字节输出流传到服务器
    BufferedOutputStream bos = new BufferedOutputStream(socket.getOutputStream());
    int b;
    while ((b=bis.read())!=-1){
        bos.write(b);
    }
    bos.flush();
    //4.当数据传入服务器给服务器结束标记
    socket.shutdownOutput();
    BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    String s;
    while ((s=br.readLine())!=null){
        System.out.println(s);
    }
    socket.close();
    bis.close();
    bos.close();
    br.close();
}
}
