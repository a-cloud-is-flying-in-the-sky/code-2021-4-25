package com.test3;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ServerTest {
    public static void main(String[] args) throws IOException {
        //4.创建服务器套接字对象用于连接客户端
        ServerSocket ss = new ServerSocket(12567);
        ThreadPoolExecutor tp = new ThreadPoolExecutor(3, 10, 2, TimeUnit.SECONDS, new ArrayBlockingQueue<>(10), Executors.defaultThreadFactory(), new ThreadPoolExecutor.AbortPolicy());
        while (true){
            //5.接收客户端传过来的数据并转为客户端套接字类型
            Socket accept = ss.accept();
            //6.创建线程对象,传入数据实现多线程传输
            Threadtest t = new Threadtest(accept);

            tp.submit(t);
        }
    }
}
