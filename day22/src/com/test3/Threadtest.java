package com.test3;

import java.io.*;
import java.net.Socket;
import java.util.UUID;

public class Threadtest implements Runnable {
    private Socket accept;
    public Threadtest(Socket accept) {
        this.accept=accept;
    }
    @Override
    public void run() {
        BufferedOutputStream bos=null;
        BufferedInputStream bis=null;
        try {
            //7.将客户端数据通过字节缓冲输入输出流复制到本地
            bis = new BufferedInputStream(accept.getInputStream());
            //UUID.randomUUID()随机产生一个唯一的序列号
            bos = new BufferedOutputStream(new FileOutputStream("day22\\"+UUID.randomUUID().toString()+".txt"));
            int b;
            while ((b=bis.read())!=-1){
                bos.write(b);
            }
            bos.flush();
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(accept.getOutputStream()));
            bw.write("操作成功");
            bw.flush();
            accept.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(bos!=null){
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(bis!=null){
                try {
                    bis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
