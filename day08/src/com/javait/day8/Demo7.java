package com.javait.day8;

import java.util.Random;
import java.util.Scanner;

public class Demo7 {
    public static void main(String[] args) {
        System.out.println(getStr());
    }
    public static void ifString(String str){
        StringBuilder str1 = new StringBuilder(str);
        str1.reverse();
        String str2=str1.toString();
        if (str.equals(str2)){
            System.out.println("对称");
        }else{
            System.out.println("不对称");
        }
    }//判断字符串是否对称
    public static void forString(){
        Scanner sc = new Scanner(System.in);
        StringBuilder str = new StringBuilder();
        int bigCount=0,smallCount=0,sumCount=0,other=0;
        for (;;){
            System.out.println("请输入一个字符串,输入end结束:");
            String a=sc.next();
            if (a.equals("end")){
                System.out.println("结束输入");
                char[] chars=str.toString().toCharArray();
                for (int i = 0; i < chars.length; i++) {
                    if (chars[i]>='a'&&chars[i]<='z'){
                        smallCount++;
                    }else if (chars[i]>='A'&&chars[i]<='Z'){
                        bigCount++;
                    }else if (chars[i]>='0'&&chars[i]<='9'){
                        sumCount++;
                    }else{
                        other++;
                    }
                }
                break;
            }else{
                str.append(a);
                System.out.println(str);
            }
        }
        System.out.println("大写字符:"+bigCount);
        System.out.println("小写字符:"+smallCount);
        System.out.println("数字字符:"+sumCount);
        System.out.println("其他字符:"+other);
    }//字符串无限拼接
    public static void phoneNum(){
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入您的电话号码:");
        String num=sc.next();
        System.out.println(num.substring(0,3)+"****"+num.substring(7));
    }//隐藏电话号码
    public static void arrPinjie(){
        int[] arr1={9,5,2,7};
        StringBuilder str = new StringBuilder();
        str.append("[");
        for (int i = 0; i < arr1.length; i++) {
            if (i==arr1.length-1){
                str.append(arr1[i]).append("]");
            }else{
                str.append(arr1[i]).append(",");
            }

        }
        System.out.println(str.toString());

    }//打印数组
    public static String getStr(){
        int shuzicount=0,zimucount=0;
        char[] arr = new char[5];
        Random random = new Random();
            for (int i = 0; i < arr.length; i++) {
                int num=random.nextInt(43)+48;
                System.out.println(num);
                if (num>=48&&num<=57){
                    if (shuzicount==1){
                        i--;
                    }else{
                        arr[i]=(char)num;
                        shuzicount++;
                    }
                }else if (num>=65&&num<=90){
                    if (zimucount==4){
                        i--;
                    }else{
                        arr[i]=(char)num;
                        zimucount++;
                    }
                }else{
                    i--;
                }
                }
        String s = new String(arr);
        return s;
    }//生成一个数字四个字母的字符串
}
