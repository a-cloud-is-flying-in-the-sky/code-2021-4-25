package com.javait.day8;

import java.util.Scanner;

public class Demo1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入字符串:");
        String s1=sc.next();
        char[] chars=s1.toCharArray();
        int bigCount=0,smallCount=0,sumCount=0,other=0;
        for (int i = 0; i < chars.length; i++) {
            if (chars[i]>='a'&&chars[i]<='z'){
                smallCount++;
            }else if (chars[i]>='A'&&chars[i]<='Z'){
                bigCount++;
            }else if (chars[i]>='0'&&chars[i]<='9'){
                sumCount++;
            }else{
                other++;
            }
        }
        System.out.println("大写字符:"+bigCount);
        System.out.println("小写字符:"+smallCount);
        System.out.println("数字字符:"+sumCount);
        System.out.println("其他字符:"+other);
    }
}
