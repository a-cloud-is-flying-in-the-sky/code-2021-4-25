package com.javait.day8;

import com.sun.org.apache.bcel.internal.generic.NEW;

import java.util.Scanner;

public class Demo6 {
    public static void main(String[] args) {
        int[] arr={1,2,3};
        System.out.println(arrString(arr));
    }
    public static String arrString(int[] arr){
        StringBuilder str = new StringBuilder();
        str.append("[");
        for (int i = 0; i < arr.length; i++) {
            if (i==arr.length-1){
                str.append(arr[i]).append("]");
            }else{
                str.append(arr[i]).append(",");
            }
        }
        return str.toString();
    }
}
