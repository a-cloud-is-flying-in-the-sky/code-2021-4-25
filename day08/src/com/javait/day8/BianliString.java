package com.javait.day8;

import java.util.Scanner;

public class BianliString {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入字符串:");
        String s1=scanner.next();
        for (int i = 0; i < s1.length(); i++) {
            System.out.print(s1.charAt(i)+" ");
        }

        System.out.println();

        char[] chars=s1.toCharArray();//toCharArray()方法将字符串对象拆分成字符数组
        for (int i = 0; i < s1.length(); i++) {//遍历该数组
            System.out.print(chars[i]+" ");
        }
    }
}
