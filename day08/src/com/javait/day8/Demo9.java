package com.javait.day8;

import java.util.Random;

public class Demo9 {
    public static void main(String[] args) {
        // 先定义String getStr()方法。
        String str = "";
        // 创建Random对象
        Random r = new Random();
        int index =r.nextInt(5);
        for (int i = 0; i < 5; i++) {
            if (i == index) {  //随机生成一个索引位置的数字
                str+=r.nextInt(10);   //0-9的数字转成字符串
            } else {
                // 如何随机生成A-Z之间的字符？     A-Z 对应的数字是 65-90
                char s = (char) (r.nextInt(26) + 65);
                str+=s;
            }
        }
        System.out.println(str);
    }

}
