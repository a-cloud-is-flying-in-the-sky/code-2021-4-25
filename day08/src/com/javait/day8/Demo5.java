package com.javait.day8;

import java.util.Scanner;

public class Demo5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入字符串");
        String s1=sc.next();
        StringBuilder stringBuilder = new StringBuilder(s1);
        stringBuilder.reverse();
        String s2=stringBuilder.toString();
        if (s1.equals(s2)){
            System.out.println("相同");
        }else{
            System.out.println("不相同");
        }

    }
}
