package com.xc.mian;

import com.xc.test.Songer;

import java.util.ArrayList;

public class TestSonger {
    public static void main(String[] args) {
        Songer songer1 = new Songer("华晨宇",21,"孤独");
        Songer songer2 = new Songer("罗志祥",29,"多人运动");
        Songer songer3 = new Songer("郭富城",15,"踢足球");
        Songer songer4 = new Songer("张学友",19,"游泳");
        ArrayList<Songer> sonarr = new ArrayList<>();
        sonarr.add(songer1);
        sonarr.add(songer2);
        sonarr.add(songer3);
        sonarr.add(songer4);
        for (int i = 0; i < sonarr.size(); i++) {
            if (sonarr.get(i).getAge()>30){
                sonarr.get(i).setLike("打保龄球");
            }else if (sonarr.get(i).getAge()<20){
                sonarr.remove(i);
                i--;
            }
        }
        for (int i = 0; i < sonarr.size(); i++) {
            System.out.println(sonarr.get(i).getAge()+sonarr.get(i).getName()+sonarr.get(i).getLike());
        }
    }
}
