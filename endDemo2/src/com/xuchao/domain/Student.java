package com.xuchao.domain;

public class Student {
    private String name;
    private String stuid;
    private int age;
    private String birthday;

    public Student(String name, String stuid, int age, String birthday) {
        this.name = name;
        this.stuid = stuid;
        this.age = age;
        this.birthday = birthday;
    }

    public Student(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStuid() {
        return stuid;
    }

    public void setStuid(String stuid) {
        this.stuid = stuid;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }
}
