package com.xuchao.test;

import com.xuchao.domain.Student;

import java.util.ArrayList;
import java.util.Scanner;

public class StuMethod {
    public void addArray(ArrayList<Student> list){
        Scanner sc = new Scanner(System.in);
        Student stu = new Student();
        System.out.println("添加学生学号：");
        stu.setStuid(sc.next());
        System.out.println("添加学生姓名：");
        stu.setName(sc.next());
        System.out.println("添加学生年龄：");
        stu.setAge(sc.nextInt());
        System.out.println("添加学生生日：");
        stu.setBirthday(sc.next());
        list.add(stu);
    }//添加
    public void removeArray(ArrayList<Student> list){
        Scanner sc = new Scanner(System.in);
        if (list.size()==0||list==null){
            System.out.println("学生信息为空，无法查询！");
        }else{
            System.out.println("请输入需要删除学生学号：");
            String id=sc.next();
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getStuid().equals(id)){
                    list.remove(i);
                }
            }
        }
    }//删除
    public void setArray(ArrayList<Student> list){
        Scanner sc = new Scanner(System.in);
        Student stu = new Student();//这里新建一个对象存放修改后的数据；
        System.out.println("请输入需要修改的学生学号：");
        String id=sc.next();
        for (int i = 0; i < list.size(); i++) {
            if (i==list.size()-1&&list.get(i).getStuid().equals(id)==false){
                System.out.println("没找到您需要修改的学号，请查看是否正确！");
                break;
            }
            if (list.get(i).getStuid().equals(id)){
                System.out.println("请输入修改后的学号：");
                stu.setStuid(sc.next());
                System.out.println("请输入修改后的姓名：");
                stu.setName(sc.next());
                System.out.println("请输入修改后的年龄：");
                stu.setAge(sc.nextInt());
                System.out.println("请输入修改后的生日：");
                stu.setBirthday(sc.next());
                list.set(i,stu);
            }
        }
    }//修改
    public void showArray(ArrayList<Student> list){
        Student stu = new Student();
        if (list.size()==0){
            System.out.println("学生信息为空，请添加");
        }else{
            System.out.println("学号\t姓名\t年龄\t生日");
            for (int i = 0; i < list.size(); i++) {
                stu=list.get(i);
                System.out.println(stu.getStuid()+"\t\t"+stu.getName()+"\t\t"+stu.getAge()+"\t\t"+stu.getBirthday());
            }
        }

    }//显示
}
