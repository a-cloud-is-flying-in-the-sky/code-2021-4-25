package com.xuchao.test;

import com.xuchao.domain.Student;

import java.util.ArrayList;
import java.util.Scanner;

public class TestStudent {
    public static void main(String[] args) {
        ArrayList<Student> list = new ArrayList<>();
        StuMethod stuMethod = new StuMethod();
        Scanner sc = new Scanner(System.in);
        for (;;){
            System.out.println();
            System.out.println("----****---欢迎来到学生管理系统---****---");
            System.out.println("1.添加学生");
            System.out.println("2.删除学生");
            System.out.println("3.修改学生");
            System.out.println("4.查看学生");
            System.out.println("5.退出");
            int num=sc.nextInt();
            if (num==1){
                stuMethod.addArray(list);
            }else if (num==2){
                stuMethod.removeArray(list);
            }else if (num==3){
                stuMethod.setArray(list);
            }else if (num==4){
                stuMethod.showArray(list);
            }else if (num==5){
                System.out.println("已退出..");
                break;
            }

        }
    }
}
