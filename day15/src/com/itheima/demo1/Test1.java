package com.itheima.demo1;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Scanner;

public class Test1 {
    public static void main(String[] args) {
        Student student = new Student();
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入姓名");
        student.setName(sc.next());
        while (true){
            System.out.println("请输入年龄:");
            try {
                int age=Integer.parseInt(sc.next());
                student.setAge(age);
                break;
            } catch (NumberFormatException e) {
                System.out.println("输入错误格式,请重新输入");
                continue;
            }catch (RuntimeException e){
                e.printStackTrace();
                continue;
            }
        }
        System.out.println(student);
    }
}
