package com.itheima.demo1;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.Scanner;

public class Demo2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入当前年份:");
        int year=sc.nextInt();
        LocalDate time = LocalDate.of(year, 3, 1);
        LocalDate time2=time.plusDays(-1);
        if (time2.getDayOfMonth()==29){
            System.out.println("是");
        }else{
            System.out.println("不是");
        }
        ;

    }
}
