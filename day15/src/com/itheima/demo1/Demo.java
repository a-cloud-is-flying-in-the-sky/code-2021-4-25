package com.itheima.demo1;

public class Demo {
    public static void main(String[] args) {
        int[] arr={8,4,7,1,3,6,5,4,1,0};
        quiteSort(arr,0,arr.length-1);
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]+" ");
        }
    }
    public static void quiteSort(int[] arr,int left,int right){
        if (left>right){
            return;
        }
        int left0=left;
        int right0=right;
        int num=arr[left0];
        while (left!=right){
            while (num<=arr[right] && left<right){//如果右边的数比基数大
                right--;
            }
            while (num>=arr[left] && left<right){//如果左边的数比基数小
                left++;
            }
            int temp=arr[left];
            arr[left]=arr[right];
            arr[right]=temp;
        }
        int temp=arr[left];
        arr[left]=arr[left0];
        arr[left0]=temp;
        quiteSort(arr,left0,left-1);
        quiteSort(arr,left+1,right0);
    }
}
