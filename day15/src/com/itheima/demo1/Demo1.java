package com.itheima.demo1;

import com.sun.org.apache.bcel.internal.generic.NEW;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class Demo1 {
    public static void main(String[] args) throws InterruptedException {

        LocalDateTime now=LocalDateTime.now();
        System.out.println(now);
        LocalDateTime now1=LocalDateTime.of(2000,8,8,23,29,59);
        DateTimeFormatter dateTimeFormatter=DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String s=now1.format(dateTimeFormatter);//将LocalDateTime类型转为格式化的String类型
        System.out.println(s);
        LocalDateTime parse=LocalDateTime.parse(s,dateTimeFormatter);//将String类型转为LocalDateTime类型
        System.out.println(parse);
        //LocalDateTime的静态方法:now(),of(),parse()
        /*while (true){
            Thread.sleep(1000);
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");
            System.out.println(sdf.format(date));
        }*/

    }
}
