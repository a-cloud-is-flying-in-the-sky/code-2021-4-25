package com.itheima.main;

import java.util.ArrayList;
import java.util.Scanner;

public class Demo2 {
    public static void main(String[] args) {
        ArrayList<String> objects = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入需要录入字符串个数:");
        int num=sc.nextInt();
        for (int i = 0; i < num; i++) {
            System.out.println("请输入第"+(i+1)+"个字符串:");
            objects.add(sc.next());
        }
        System.out.println("请输入需要删除的字符串:");
        String s=sc.next();
        for (int i = 0; i < objects.size(); i++) {
            if (s.equals(objects.get(i))){
                objects.remove(s);
                i--;
            }
        }
        System.out.println(objects);
    }
}
