package com.itheima.main;

import com.itheima.domain.Student;

import javax.jws.soap.SOAPBinding;
import java.util.ArrayList;
import java.util.Scanner;

public class Teststudent {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入需要录入学生信息个数:");
        int num=sc.nextInt();
        ArrayList<Student> objects = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            System.out.println("第"+(i+1)+"名学生姓名:");
            String name=sc.next();
            System.out.println("第"+(i+1)+"名学生年龄:");
            int age=sc.nextInt();
            Student student = new Student(name,age);
            objects.add(student);
        }
        for (int i = 0; i < objects.size(); i++) {
            System.out.println(objects.get(i).getName()+"..."+objects.get(i).getAge());
        }
    }
}
