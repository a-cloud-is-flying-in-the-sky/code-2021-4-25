package com.itheima.manage.controller;

import com.itheima.manage.domain.Student;
import com.itheima.manage.service.StudentService;

import java.util.Scanner;

public class ControllerBich {
    private Scanner sc = new Scanner(System.in);
    private StudentService studentService = new StudentService();
    public void bichview(){
        lo:while (true){
            System.out.println("----技师管理----");
            System.out.println("1.添加技师信息\t2.删除技师信息\t3.修改技师信息\t4.查询技师信息\t4.退出" );
            String i=sc.next();
            switch (i){
                case "1":
                    addbich();
                    break;
                case "2":
                    remove();
                    break;
                case "3":
                    seting();
                    break;
                case "4":
                    findbich();
                    break;
                case "5":
                    System.out.println("退出技师管理");
                    break lo;
                default:
                    System.out.println("输入有误!");
                    break;
            }
        }
    }//技师主界面
    public void addbich(){
        Student stu = new Student();
        String id;
        while (true){
            System.out.println("请输入技师id:");
            id=sc.next();
            if (studentService.isExists(id)){//1.判断添加id是否存在
                System.out.println("id已存在");
            }else{
                break;
            }
        }
        stu.setId(id);
        System.out.println("请输入技师姓名:");
        stu.setName(sc.next());
        System.out.println("请输入技师年龄:");
        stu.setAge(sc.nextInt());
        System.out.println("请输入技师生日:");
        stu.setBirthday(sc.next());
        boolean ifbich=studentService.addbich(stu);
        if (ifbich){
            System.out.println("添加成功!");
        }else{
            System.out.println("存储空间已满,添加失败...");
        }

    }//添加
    public void remove(){
        String id;
        while (true){
            if (studentService.allarr()==null){
                System.out.println("查无数据请添加");
                break;
            }
            System.out.println("请输入需要删除的技师id:");
            id=sc.next();
            if (studentService.isExists(id)){
                studentService.remove(id);
                break;
            }else{
                System.out.println("id不存在,请重新输入");
            }
        }
    }//删除
    public void seting(){
        Student student = new Student();
        String id;
        while (true){
            System.out.println("请输入需要修改的id;");
            id=sc.next();
            if (studentService.isExists(id)){
                System.out.println("修改的id:");
                student.setId(sc.next());
                System.out.println("修改的姓名:");
                student.setName(sc.next());
                System.out.println("修改的年龄:");
                student.setAge(sc.nextInt());
                System.out.println("修改的生日:");
                student.setBirthday(sc.next());
                studentService.setting(id,student);
                System.out.println("修改成功");
                break;
            }else{
                System.out.println("id不存在,请重新输入");
            }
        }


    }//修改
    public void findbich(){
        Student[] stu=studentService.allarr();
        if (stu==null){
            System.out.println("查询为空,请添加");
        }else{
            System.out.println("id\t\t姓名\t年龄\t生日");
            for (int i = 0; i < stu.length; i++) {
                    if (stu[i]==null){
                        break;
                    }
                System.out.println(stu[i].getId()+"\t\t"+stu[i].getName()+"\t"+stu[i].getAge()+"\t\t"+stu[i].getBirthday());
            }
        }
    }//查询
}
