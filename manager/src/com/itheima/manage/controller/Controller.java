package com.itheima.manage.controller;

import java.util.Scanner;

public class Controller {
    public void view(){
        Scanner sc = new Scanner(System.in);
        ControllerBich controllerBich = new ControllerBich();
        ControllerCustom controllerCustom = new ControllerCustom();
        while (true){
            System.out.println("-----欢迎来到技师管理系统-----");
            System.out.println("1.技师管理\t2.客户管理\t3.退出系统");
            String i=sc.next();
            switch (i){
                case "1":
                    controllerBich.bichview();
                    break;
                case "2":
                    controllerCustom.customview();
                    break;
                case "3":
                    System.exit(0);//跳出虚拟机
                default:
                    System.out.println("输入有误,请重新输入");
                    break;
            }
        }
    }
}
