package com.itheima.manage.service;

import com.itheima.manage.dao.StudentDao;
import com.itheima.manage.domain.Student;

import java.util.Arrays;

public class StudentService {
    public boolean addbich(Student stu){
    StudentDao studentDao = new StudentDao();
    Student[] students=studentDao.allarr();
        int index=-1;
        for (int i = 0; i < students.length; i++) {//判断数组空间是否足够
            if (students[i]==null){
                index=i;
                students[index]=stu;
                break;
            }
        }
        if (index==-1){
            return false;
        }else{
            studentDao.addbich(stu,index);
            return true;
        }
}//添加
    public boolean isExists(String id){
        StudentDao studentDao = new StudentDao();
        Student[] stu=studentDao.allarr();
        for (int i = 0; i < stu.length; i++) {
            if (stu[i]!=null && stu[i].getId().equals(id)){
                return true;
            }
        }
        return false;
    }//判断id是否存在
    public Student[] allarr(){
        StudentDao dao = new StudentDao();
        Student[] arr=dao.allarr();
        boolean ifarr=false;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i]!=null){
                ifarr=true;
                break;
            }
        }
        if (ifarr){
            return arr;
        }else{
            return null;
        }
    }//得到数组,遍历数组判断数组内是否有值,有则返回数组,没有返回null
    public void remove(String id){
        StudentDao dao = new StudentDao();
        dao.remove(id);
    }//删除
    public void setting(String id,Student stu){
        StudentDao dao = new StudentDao();
        Student[] students=dao.allarr();
        for (int i = 0; i < students.length; i++) {
            if (students[i]!=null&&students[i].getId().equals(id)){
                dao.setting(i,stu);
                break;
            }
        }
    }//修改
}
